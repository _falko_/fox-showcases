package com.camunda.fox.showcase.invoice.de;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import org.activiti.engine.HistoryService;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.runtime.Execution;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.DependencyResolvers;
import org.jboss.shrinkwrap.resolver.api.maven.MavenDependencyResolver;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.camunda.fox.showcase.invoice.de.ProcessConstants;

@RunWith(Arquillian.class)
public class ArquillianTestCase {

  @Deployment  
  public static WebArchive createDeployment() {
    MavenDependencyResolver resolver = DependencyResolvers.use(MavenDependencyResolver.class).loadMetadataFromPom("pom.xml");
    
    return ShrinkWrap.create(WebArchive.class, "test.war")
            // prepare as process application archive for fox platform
//            .addAsManifestResource("ARQUILLIAN-MANIFEST-JBOSS7.MF", "MANIFEST.MF")
            .addAsLibraries(resolver.artifact("com.camunda.fox:fox-platform-client").resolveAsFiles())
            .addAsWebResource("META-INF/processes.xml", "WEB-INF/classes/META-INF/processes.xml")
            // add your own classes (could be done one by one as well)
            .addPackages(true, "com.camunda.fox.invoice")
            // add process definition
            .addAsResource("diagrams/invoice.bpmn20.xml")
            // now you can add additional stuff required for your test case
            ;    
  }

  @Inject
  private ProcessEngine processEngine;

  @Inject
  private RuntimeService runtimeService;

  @Inject
  private TaskService taskService;
  
  @Inject
  private HistoryService historyService;

  @Test
  public void test() throws InterruptedException {
    HashMap<String, Object> variables = new HashMap<String, Object>();
    
    ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("invoice", variables);
    String id = processInstance.getId();
    System.out.println("Started process instance id " + id);
    
    List<String> activityIds = runtimeService.getActiveActivityIds(id);
    
    assertEquals(1, activityIds.size());
    assertEquals("Freigebenden_zuordnen_143", activityIds.get(0));
    
   
  }
}
