package com.camunda.fox.showcase.invoice.de;


public class ProcessConstants {

  public static final String PROCESS_NAME_ADONIS = "adonis-invoice";
  public static final String PROCESS_NAME_FOX = "fox-invoice";
  
  public static final String VARIABLE_INVOICE = "invoice";

}
