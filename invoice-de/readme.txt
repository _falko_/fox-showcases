# Introduction
This showcase shows an invoice inbound process in German ("Rechnungseingang") including a JSF + JQuery web-gui.
It not only deploys a process model from the fox designer but as well one from Adonis, another BPMN 2.0 Modeling tool. 


# Environment Restrictions
None known


# Remarks to run this quickstart
After deploying the invoice-de.war file to your fox platform you can access the application: http://localhost:8080/invoice-de/taskList.jsf. 


# Known Issues:
* Deployment of Adonis model yields in exception but doesn't harm execution: https://app.camunda.com/jira/browse/HEMERA-2203 